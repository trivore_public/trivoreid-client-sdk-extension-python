## TrivoreID Client SDK Extension

The extended version of the Client SDK that wraps around the TrivoreID APIs.

The most essential endpoints are covered in the [basic version](https://gitlab.com/trivore_public/trivoreid-client-sdk-python). Use it
if you need a lightweight client.

Documentation to get started and use Services can be found [here](https://trivore.atlassian.net/wiki/spaces/TISpubdoc/pages/20515307/Client+SDK+for+Python).

### PyPi Package

History of all releases can be found [here](https://pypi.org/project/trivoreid-extended/).

Installing with pip:

```
pip install trivoreid-extension
```
